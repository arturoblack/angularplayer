var app = angular.module("musicApp", []);
app.controller("mainCtrl",
  [
    '$scope',
    'musicService',
    function($scope, musicService) {
      $scope.listaMusica = musicService;
    }
  ]
);
